import React from 'react';
let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
]
class DaftarHargaBuah extends React.Component{
    render(){
        return(
            <div style={{fontFamily:"serif"}}>
                 
                <h1 style={{textAlign:"center"}}>Tabel Harga Buah</h1>
                <table style={{margin:"0 auto", border: "1px solid black", width:"700px", height:"0px"}}>
                    <tr>
                        <th style={{backgroundColor:"#aaaaaa"}}>Nama</th>
                        <th style={{backgroundColor:"#aaaaaa"}}>Harga</th>
                        <th style={{backgroundColor:"#aaaaaa"}}>Berat</th>
                    </tr>

            {
                dataHargaBuah.map(el=>{
                    return(
                        <tr>
                            <td style={{backgroundColor:"#ff7f50"}}>{el.nama}</td>
                            <td style={{backgroundColor:"#ff7f50"}}>{el.harga}</td>
                            <td style={{backgroundColor:"#ff7f50"}}>{el.berat/1000+' kg'}</td>
                        </tr>
                    )
                })
            }
            </table>
            </div>
        )
    }
}
export default DaftarHargaBuah;