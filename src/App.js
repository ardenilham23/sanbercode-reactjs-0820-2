import React from 'react';
//import logo from './logo.svg';
//import PenjualanBuah from './Tugas-9/tugas9.js';
//import DaftarHargaBuah from './Tugas-10/tugas10.js';
//import Timer from './Tugas-11/tugas11.js';
//import FormBuah from './Tugas-12/tugas12.js';
//import BuahAxios from './Tugas-13/tugas13.js';
//import Movie from './Tugas-14/Movie' ;
//import Buah from './Tugas-14/Buah';
import './App.css';
import { BrowserRouter as Router } from "react-router-dom";
import Nav from './Tugas-15/Nav'
import Routes from './Tugas-15/Routes'



function App() {
  return (
    <div>
      {/*<PenjualanBuah />
    
      <DaftarHargaBuah />
      <br/>
      <br/>
      <Timer start={101}/>
      <FormBuah />
      <Movie/>
      <BuahAxios />
      <Buah/>*/}
      <Router>
        <Nav />
        <Routes />
      </Router>
    </div>
  );
}

export default App;
