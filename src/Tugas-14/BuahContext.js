import React, { useState, createContext } from "react";


export const BuahContext = createContext();

export const BuahProvider = props => {
    const [dataHargaBuah, setBuah] = useState(null);
    const [input, setInput] = useState({
      name: "",
      price: "",
      weight: 0,
      id: null,
    });

  return (
    <BuahContext.Provider value={[dataHargaBuah, setBuah, input, setInput]}>
      {props.children}
    </BuahContext.Provider>
  );
};
