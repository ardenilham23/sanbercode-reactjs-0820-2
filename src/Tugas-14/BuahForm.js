import React, {useContext, useState} from "react"
import {BuahContext} from "./BuahContext"
import axios from "axios";


const FormBuah = () =>{
    const [dataHargaBuah, setBuah, input, setInput] = useContext(BuahContext)
  
const myChangeHandle = (event) =>{
        let nam = event.target.name;
        var value = event.target.value;
        console.log(value);
        setInput({ ...input, [nam]: value });
      };

const submitForm = (event) => {
        event.preventDefault();
        if (input.id === null) {
          axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {
              name: input.name,
              price: input.price,
              weight: input.weight,
            })
            .then((res) => {
              let data = res.data;
            setBuah([
                  ...dataHargaBuah,
                {
                  name: input.name,
                  price: input.price,
                  weight: input.weight,
                  id: data.id,
                },
              ]);
            });
    
        } else {
          axios.put(`http://backendexample.sanbercloud.com/api/fruits/${input.id}`, {
              name: input.name,
              weight: input.weight,
              price: input.price,
            })
            .then((res) => {
              var newDataBuah = dataHargaBuah.map((x) =>{
                if (x.id === input.id) {
                  x.name = input.name;
                  x.price = input.price;
                  x.weight = input.weight;
                  setBuah([...dataHargaBuah])
                }
              });
            });
        }

      setInput({name: "", price: "", weight: 0, id: null});
    }

return(
    <>
    {/* Form Buah*/}
      <form onSubmit={submitForm}>
        <h1>Form Input Buah</h1>
        <label>Nama Buah :</label>
        <input require type="text"
          name="name"
          value={input.name}
          onChange={myChangeHandle}
        />
        <label>Harga Buah :</label>
        <input
          required
          type="text"
          name="price"
          value={input.price}
          onChange={myChangeHandle}
        />
        <label>Berat (gram) :</label>
        <input
          required
          type="number"
          name="weight"
          value={input.weight}
          onChange={myChangeHandle}
        />
        <button className="submit">Submit</button>
      </form>
    </>
    )
}


export default FormBuah;
