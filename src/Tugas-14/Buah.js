import React from "react"
import {BuahProvider} from "./BuahContext"
import TabelBuah from "./BuahTabel"
import FormBuah from "./BuahForm"


const Buah = () =>{
  return(
    <BuahProvider>
        <TabelBuah/>
        <FormBuah/>
    </BuahProvider>
  )
}


export default Buah;
