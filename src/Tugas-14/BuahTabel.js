import React, {useContext, useEffect} from "react"
import {BuahContext} from "./BuahContext"
import axios from "axios";


const TabelBuah = () =>{
    const [dataHargaBuah, setBuah, input, setInput] = useContext(BuahContext);
  
useEffect(() =>{
    if (dataHargaBuah === null) {
      axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
        .then((res) => {
            setBuah(
                res.data.map((el) =>{
                    return{
                        name: el.name,
                        price: el.price,
                        weight: el.weight,
                        id: el.id,
                    };
                })
            );
        });
    }
  }, [dataHargaBuah]);

const editForm = (event) => {
    const idBuah = parseInt(event.target.value);
    console.log(idBuah);
    var buah = dataHargaBuah.find((x) => x.id === idBuah);
    setInput({
      id: idBuah,
      name: buah.name,
      price: buah.price,
      weight: buah.weight,
    });
  };

const deleteForm = (event) => {
    var idBuah = parseInt(event.target.value);
    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
      .then((res) => {
        var newDataBuah = dataHargaBuah.filter((x) => x.id !== idBuah);
        setBuah([...newDataBuah]);
      });
  };

return (
    <div>
        {/* Tabel Buah*/}
      <h1>Tabel Harga Buah</h1>
      <table className="tabelBuah">
        <thead>
          <tr>
            <th>Nama</th>
            <th>Harga</th>
            <th>Berat</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          {dataHargaBuah !== null &&
            dataHargaBuah.map((el, index) => {
              return (
                <tr key={el.id}>
                  <td>{el.name}</td>
                  <td>{el.price}</td>
                  <td>{el.weight / 1000} kg</td>
                  <td>
                    <button value={el.id} style={{ marginRight: "10px" }} onClick={editForm}>Edit</button>
                    <button value={el.id} onClick={deleteForm}>Delete</button>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
      </div>
  );
}


export default TabelBuah;
