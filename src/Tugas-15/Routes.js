import React from "react";
import PenjualanBuah from '../Tugas-9/tugas9'
import DaftarHargaBuah from '../Tugas-10/tugas10'
import Timer from '../Tugas-11/tugas11'
import FormBuah from '../Tugas-12/tugas12'
import BuahAxios from '../Tugas-13/tugas13'
import Buah from '../Tugas-14/Buah'
import { Switch, Route } from "react-router";

const Routes = () => {

  return (
    <Switch>
      <Route exact path="/">
        <Buah />
      </Route>
      <Route path="/tugas13">
        <BuahAxios />
      </Route>
      <Route exact path="/tugas12">
        <FormBuah />
      </Route>
      <Route exact path="/tugas11">
        <Timer start={101}/>
      </Route>
      <Route path="/tugas10">
        <DaftarHargaBuah />
      </Route>
      <Route exact path="/tugas9">
        <PenjualanBuah />
      </Route>
    </Switch>
  );
};

export default Routes;
