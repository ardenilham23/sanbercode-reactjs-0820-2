import React, { Component, Children } from 'react';
import './form.css'

class FormBuah extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dataHargaBuah: [
                {
                    nama: "Semangka", 
                    harga: 10000, 
                    berat: 1000
                },
                {
                    nama: "Anggur", 
                    harga: 40000, 
                    berat: 500
                },
                {
                    nama: "Strawberry", 
                    harga: 30000, 
                    berat: 400
                },
                {
                    nama: "Jeruk", 
                    harga: 30000, 
                    berat: 1000
                },
                {
                    nama: "Mangga", 
                    harga: 30000, 
                    berat: 500
                }
            ],
            inputNama: "",
            inputHarga: 0,
            inputBerat: 0,
            indexofForm: -1
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleSubmit(event){
        event.preventDefault();
        let buah = {
            "nama": this.state.inputNama,
            "harga": this.state.inputHarga,
            "berat": this.state.inputBerat
        }

        if (this.state.indexofForm !== -1) {
            let newBuah = [...this.state.dataHargaBuah];
            newBuah.splice(this.state.indexofForm, 1, buah);
            this.setState({
                dataHargaBuah: newBuah,
                inputNama: "",
                inputHarga: 0,
                inputBerat: 0,
                indexofForm: -1
            })
            return;
        } 
        this.setState({
            dataHargaBuah: [...this.state.dataHargaBuah, buah],
            inputNama: "",
            inputHarga: 0,
            inputBerat: 0,
            index: -1
        });
    }

    handleChange(event){
        const val = event.target.value;
        this.setState({
            [event.target.name]: val
        });
    }
    
    handleEdit(event){
        let i = event.target.value;
        let data = this.state.dataHargaBuah[i];
        this.setState({
            inputNama: data.nama,
            inputHarga: data.harga,
            inputBerat: data.berat,
            indexofForm: i
        })
    }

    handleDelete(event){
        let i = event.target.value;
        let data = this.state.dataHargaBuah;
        data.splice(i, 1);

        this.setState({
            ...this.state, dataHargaBuah: data
        });
    }

    render(){
        return (
            <>
            <center>
            <h1>Tabel Harga Buah</h1>
                <table style={{width:"50%", border: "1px solid black"}}>
                    <thead>
                        <tr style={{backgroundColor:"#aba9aa"}}>
                            <th>Nama</th>
                            <th>Harga</th>
                            <th>Berat</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.state.dataHargaBuah.map( (e, i) => {
                        return (
                            <tr key={i} style={{backgroundColor: "#ff7f50"}}>
                                <td>{e.nama}</td>
                                <td>{e.harga}</td>
                                <td>{e.berat / 1000} kg</td>
                                <td>
                                    <button value={i} onClick={this.handleEdit}>
                                        Edit
                                    </button>
                                    <button value={i} onClick={this.handleDelete}>
                                        Hapus
                                    </button>
                                </td>
                            </tr>
                        )
                    })}
                    </tbody>
                </table>
            </center>
            <h1>Form Buah</h1>
            <form onSubmit={this.handleSubmit}>
                <label>
                    Masukan Nama Buah:
                </label>
                <input 
                    type="text" 
                    name="inputNama" 
                    value={this.state.inputNama} 
                    onChange={this.handleChange}
                />
                <br />
                <label>
                    Masukan Harga Buah:
                </label>
                <input 
                    type="text" 
                    name="inputHarga" 
                    value={this.state.inputHarga} 
                    onChange={this.handleChange}
                />
                <br />
                <label>
                    Masukan Berat Buah:
                </label>
                <input 
                    type="text" 
                    name="inputBerat" 
                    value={this.state.inputBerat} 
                    onChange={this.handleChange}
                />
                <br />
                <button>Submit</button>
            </form>
            </>
        )
    }
}

export default FormBuah;
